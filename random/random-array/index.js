function getRandomItem(data) {
    const randomIndex = Math.floor(Math.random() * data.length);
    const item = data[randomIndex];
    return item;
}

const motor = ['Vario', 'Beat', 'Mio', 'Scoopy'];

const result = getRandomItem(motor);
console.log(result);