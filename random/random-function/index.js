// Fungsi JavaScript ini selalu mengembalikan angka acak antara min (termasuk) dan max (tidak termasuk)
function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
// di acak 1 sampai 9
console.log(randomInt(1, 10))

// Fungsi JavaScript ini selalu mengembalikan angka acak antara min dan max (keduanya disertakan):
function randomInt2(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
//  di acak 1 sampai 10
console.log(randomInt2(1, 10))