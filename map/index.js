let map = new Map();

map.set('1', 'string'); // kuncinya berupa string
console.log(map.get('1')) // string

map.set(1, 'angka'); // kuncinya berupa number
console.log(map.get(1)) // angka

map.set(true, 'boolean'); // kuncinya berupa boolean
console.log(map.get(true)) // boolean

// var.size adlh mengetahui berapa banyak yg di get
console.log(map.size) // 3

/* 
    tutorial lanjutnya :
    https://id.javascript.info/map-set
*/