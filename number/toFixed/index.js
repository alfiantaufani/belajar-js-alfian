console.log("Number.toFixed() mengeset jumlah desimal di belakang koma dan dilakukan pembulatan")
let angka = 9.56;
console.log("Angka awal: " + angka);
console.log("Angka toFixed(0): " + angka.toFixed(0)) // 10
console.log("Angka toFixed(1): " + angka.toFixed(1)) // 9.5
console.log("Angka toFixed(2): " + angka.toFixed(2)) // 9.56
console.log("Angka toFixed(3): " + angka.toFixed(3)) // 9.560