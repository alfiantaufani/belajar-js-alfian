console.log("Contoh fungsi parseFloat() untuk mengubah String menjadi Integer (sama seperti parseInt()) ");

console.log(parseFloat("20")) // 20
console.log(parseFloat("20.45")) // 20.45
console.log(parseFloat("20 30 40")) // 20
console.log(parseFloat("20 Tahun")) // 20
console.log(parseFloat("String")) // NaN