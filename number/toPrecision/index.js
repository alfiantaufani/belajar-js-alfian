console.log("Contoh Fungsi Number.toPrecision()");
let angka = 34.567;
console.log("Angka awal: " + angka);
console.log("Angka toPrecision(): " + angka.toPrecision()) // 34.567
console.log("Angka toPrecision(2): " + angka.toPrecision(2)) // 35
console.log("Angka toPrecision(4): " + angka.toPrecision(4)) // 34.57
console.log("Angka toPrecision(6): " + angka.toPrecision(6)) // 34.5670