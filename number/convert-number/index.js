console.log("Contoh Convert to Number(param)");

console.log(Number(true)) // 1
console.log(Number(false)) // 0
console.log(Number("100")) // 100
console.log(Number("  100")) // 100
console.log(Number("100.4")) // 100.4
console.log(Number("100 300")) // NaN
console.log(Number("String")) // NaN