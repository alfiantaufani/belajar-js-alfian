console.log("Contoh fungsi parseInt() untuk merubah data menjadi tipe data Integer");

console.log(parseInt("-39"))        // -39
console.log(parseInt("-39.67"))     // -39
console.log(parseInt("39"))         // 39
console.log(parseInt("39.67"))      // 39
console.log(parseInt("39.5 4.6 7")) // 39
console.log(parseInt("39 Tahun"))   // 39
console.log(parseInt("Tahun 39"))   // NaN