async function myDisplay() {
    let myPromise = new Promise(function(resolve, reject) {
        resolve("Hello");
    });
    console.log(await myPromise) ;
}

myDisplay();
