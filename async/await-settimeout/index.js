async function myDisplay() {
    let myPromise = new Promise(function(resolve) {
        setTimeout(function() {resolve("I love You !!");}, 1000);
    });
    console.log(await myPromise);
}
myDisplay();