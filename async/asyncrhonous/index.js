function getData(url, success, error) {
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200){
                success(xhr.response)
            }else if (xhr.status == 400){
                error();
            }
        }
    }

    xhr.open('get', url);
    xhr.send();
}

console.log("Mulai")
getData('async/syncrhonous/data.json', (hasil) => {
    const data = JSON.parse(hasil);
    data.forEach(m => {
        console.log(m.nama)
    });
}, () => {

});
console.log("Selesai")