const angka = [3, -6, 8, -3, 1, 2];

function desimal(angka, callback) {
    const array = []
    for (let b of angka) {  
        if (callback(b)) {
            array.push(b)
        }      
    }
    return array;
}

// panggil fungsi beserta callback
const cekAngka = desimal(angka, (a) => a >= 0);

// akses
console.log(cekAngka) 