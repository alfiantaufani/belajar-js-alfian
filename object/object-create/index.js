/*
    - Object.create() adalah membuat function object dari luar function.
    - Object.create solusi untuk function declarative, karena dengan memisah
      method object ygn terdapat di dalam function
    - Dari penerapan ini sangat efisien dalam mengatur memori di bandingkan
      dengan Object Declarative bukan dengan constructor, 
      karena object create masih menggunakan return
*/

// object declaration
const methodAnak = {
    makan: function (porsi) {
        this.energi += porsi
    },
    bermain: function (jam) {
        this.energi -= jam
    },
    tidur: function (jam) {
        (this.energi += jam) * 2
    }
}

// function declaration
function anak(nama, energi) {
    let anak = Object.create(methodAnak);
    anak.nama = nama;
    anak.energi = energi;

    return anak;
}

let andi = anak('andi', 10)