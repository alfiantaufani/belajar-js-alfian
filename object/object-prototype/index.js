/*
    - Prototype adalah memisahkan object function yg ada di dalam object
      dengan mengganti menjadi prototype bukan dengan object lagi.
    - Penggunaan Prototype menjadi solusi untuk oject Constructor.
    - dari penerapan ini sangat efisien dalam mengatur memori dibandingkan dengan
      menggunakan Object Constructor;
*/

function Anak(nama, energi) {
    this.nama = nama;
    this.energi = energi;
}

// Protoype nya
Anak.prototype.makan = function (porsi) {
    this.energi += porsi;
    return `${this.nama} berhasil makan.`
}

Anak.prototype.bermain = function (jam) {
    this.energi -= jam;
    return `${this.nama} sedang bermain.`
}

Anak.prototype.tidur = function (jam) {
    this.energi += jam * 2;
    return `${this.nama} sedang tidur.`
}

let andi = new Anak("Andi", 20)