/*
    - object constructor sangat disaranka,
    - object ini tidak perlu menggunkan return,
    - dari penerapan ini sangat efisien dalam mengatur memori di bandingkan
      dengan Object Declarative
*/

// contoh 1 dengan object
function Anak(nama, energi) {
    this.nama = nama;
    this.energi = energi;

    this.makan = function (porsi) {
        this.energi += porsi
    }

    this.bermain = function (jam) {
        this.energi -= jam
    }
}

let andi = new Anak("Andi", 20)