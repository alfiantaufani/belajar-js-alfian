console.log("Contoh Object :")

const object = {
    property : "Value"
}

console.log(object); // {property: 'Value'}
console.log(object.property) // Value
console.log(object["property"]) // Value