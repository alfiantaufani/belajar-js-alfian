console.log("Object Method ialah object yang bisa di beri fungsi di dalam propertinya");

const object = {
    property1: "Value 1",
    property2: "Value 2",
    property3: function () {
        return this.property1 + " dan " + this.property2
    }
}

console.log(object.property1) // Value 1
console.log(object.property3()) // Value 1 dan Value 2