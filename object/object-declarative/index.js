/*
    object deklarativ harus menggunakan return
*/

function Anak(nama, energi) {
    let anak = {};
    anak.nama = nama;
    anak.energi = energi;

    anak.makan = function (porsi) {
        this.energi += porsi
    }

    anak.bermain = function (jam) {
        this.energi -= jam
    }

    return anak
}

let andi = Anak('And', 10);