const motor = {
    nama: "Honda Vario",
    warna: "white",
    berat: 40,
    start: function () {
        console.log("Fungsi start jalan")
    },
    stop: function (param) {
        console.log("Motor berhenti di kilometer: " + param + " km");
    }
}

// Cara menjalankan :
console.log(motor.nama) // Honda Vario
motor.start() // Fungsi start jalan
motor.stop(12) // Motor berhenti di kilometer: 12 km