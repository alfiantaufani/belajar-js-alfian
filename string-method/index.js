// Length
let text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
let length = text.length;
console.log(length) // 26


// Extract string
// fungsi slice(start, end)
// let buah = "Apple, Banana, Kiwi";
let buah = "I can eat bananas all day";
let part1 = buah.slice(10,17);
let part2 = buah.slice(4);
let part3 = buah.slice(-10);
let part4 = buah.slice(-12, -2);

console.log(part1) // pple, Banana
// console.log(part2) // e, Banana, Kiwi
// console.log(part3) // nana, Kiwi
// console.log(part4) // Banana, Ki

// substring(start, end)
let fruit = "Apple, Banana, Kiwi";
let fruits = fruit.substring(0, 13);
console.log(fruits)

// substr(start, length)
let animal = "Kelinci, Sapi, Kambing";
let animals = animal.substr(0, 10);
console.log(animals) //Kelinci, S

// replace()
let txt = "Kata diganti";
let newtxt = txt.replace("diganti", "W3Schools");
console.log(newtxt) // Kata W3Schools

let var_txt_1 = "Hari ini"
let txt_awal = "Kata " + var_txt_1;
let txt_hasil = txt_awal.replace(var_txt_1, "Diganti ini");
console.log(txt_hasil) // Kata Diganti ini

let txt_diganti_1 = "Please visit Microsoft and Microsoft!";
let newtxt_diganti_1 = txt_diganti_1.replace("Microsoft", "W3Schools"); // diganti hanya satu kata yg sama
console.log(newtxt_diganti_1) // Please visit W3Schools and Microsoft!

let txt_diganti_2 = "Please visit Microsoft and Microsoft!";
let newtxt_diganti_2 = txt_diganti_2.replace(/Microsoft/g, "W3Schools"); // diganti semua kata yg sama
console.log(newtxt_diganti_2) // Please visit W3Schools and W3Schools!

// replaceAll()
let txt_replaceAll = "Please visit Microsoft and Microsoft!";
let new_txt_replaceAll = txt_replaceAll.replaceAll('Microsoft', 'Kata ganti');
console.log(new_txt_replaceAll) // Please visit Kata ganti and Kata ganti!

let txt_replaceAll_global = "Please visit Microsoft and Microsoft dan Microsoft itu Emang gitu!";
let new_txt_replaceAll_global = txt_replaceAll_global.replaceAll(/Microsoft/g, 'Kata ganti');
console.log(new_txt_replaceAll_global) // Please visit Kata ganti and Kata ganti dan Kata ganti itu Emang gitu!

// concat()
let text1 = "Hello";
let text2 = "World";
let text3 = text1.concat(" dan ", text2);
console.log(text3)

// trim() : menghilangkan spasi diawal dan akhir
let texts = "    Hello World!      ";
let text_trim1 = texts.trim();
let text_trim2 = texts.trimStart();
console.log(text_trim1)
console.log(text_trim2)

// padStart(jumlah_karakter, isi_karakter) & padEnd() : menambah karakter di depan dan di akhir
let text_pad = "5";
let padded = text_pad.padStart(5,"0");
let paddeds = text_pad.padEnd(5,"0");
console.log(padded)
console.log(paddeds)

// charAt(karakter_keberapa)
let textChar = "HELLO WORLD";
let char1 = textChar.charAt(1);
let char2 = textChar[1];
console.log(char1) // E
console.log(char1) // E

// split() : ubah karakter menjadi array
let text_split1 = textChar.split("|"); // stau string satu array
let text_split2 = textChar.split(""); // satu huruf satu array
console.log(text_split1)
console.log(text_split2)