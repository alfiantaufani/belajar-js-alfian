/*
    - Perbedaan forecah dari yg lainnya adalah foreach tidak bisa 
      melooping data string, hanya data array saja yg bisa.
    
*/

const sepeda = ["Vario", "Beat", "Scoopy"];
sepeda.forEach(value => { // belum ada indexnya
    console.log(value)
}) 

sepeda.forEach((value, index) => { // sudah ada index
    console.log(`Sepeda ${value} urutan ke-${index + 1}`)
})