/*
    - Default For in memiliki index
    - For in disarankan untuk me looping data object
*/

console.log("Contoh penggunaan Iterasi Loopong (For In)");

const arr = ["Vario", "Beat", "Scoopy"];
const sepeda = {merk: "Honda", tahun: 2015, brand: "Vario"};


for (let a in sepeda) {
    console.log(`index = ${a}, value = ${sepeda[a]}`)
}