/*
    - Default dari for of tidak memiliki index,
    - Untuk menampilkan index menambahkan .entries() saat looping
    - For of bisa melooping data iterables (array, string, argumen, typedArray, Map, Set, User Defined iterables)
    - For of tidak bisa me looping sebuah object
*/

console.log("Contoh penggunaan Iterasi Loopong (For Of)");

const sepeda = ["Vario", "Beat", "Scoopy"];

// belum ada index
for (let a of sepeda) {
    console.log(a) // Vario
    console.log(sepeda) // ["Vario", "Beat", "Scoopy"]
}

// membuat index nya
for (const [index, value] of sepeda.entries()) {
    console.log(`Sepeda ${value} urutan ke-${index + 1}`)
}

// melooping dari argumen
function JumlhakanArr() {
    let arg = 0;
    for (const a of arguments) {
        arg += a
    }
    return arg
}

console.log(JumlhakanArr(1,3,2,4,5))