console.log("Array manipulation adalah Manipulasi array memungkinkan Anda melakukan tugas seperti menambah, menghapus, atau mengubah elemen dalam array")
const data = ['value 1', 'value 2', 'value 3'];

data[0] = "data depan"; // merubah data awal
data[data.length-1] = "data akhir" // merubah data terakhir

console.log(data) // ['data depan', 'value 2', 'data akhir']