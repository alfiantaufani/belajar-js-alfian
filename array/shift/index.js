// shift() menghapus elemen array pertama dan "menggeser"
const buah = ["Banana", "Orange", "Apple", "Mango"];
console.log(buah.shift()) // Banana
console.log(buah) // ['Orange', 'Apple', 'Mango']