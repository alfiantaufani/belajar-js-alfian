// concat() membuat array baru dengan menggabungkan (menggabungkan) array yang sudah ada
const girl = ["Nanda", "Anggi"];
const boy = ["Willi", "Robi"];
console.log(girl.concat(boy)) // ['Nanda', 'Anggi', 'Willi', 'Robi']

// concat() lebih dari 2
const arr1 = ["Cecilie", "Lone"];
const arr2 = ["Emil", "Tobias", "Linus"];
const arr3 = ["Robin", "Morgan"];
console.log(arr1.concat(arr2, arr3)) // ['Cecilie', 'Lone', 'Emil', 'Tobias', 'Linus', 'Robin', 'Morgan']

console.log(girl.concat("Roy")) // ['Nanda', 'Anggi', 'Roy']