//  Array pop() menghapus elemen terakhir dari array
const fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruits.pop()) // Mango
console.log(fruits) // ['Banana', 'Orange', 'Apple']