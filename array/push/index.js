// push() menambahkan elemen baru ke array (di akhir)
const fruit = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruit.push("Kiwi")) // 5
console.log(fruit) // ['Banana', 'Orange', 'Apple', 'Mango', 'Kiwi']