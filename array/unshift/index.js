// unshift() menambahkan elemen baru ke array (di awal), dan "membatalkan pergeseran" elemen lama atau me replace array pertama
const unshift = ["Banana", "Orange", "Apple", "Mango"];
console.log(unshift.unshift("Blimbing")) // 5
console.log(unshift) // ['Blimbing', 'Banana', 'Orange', 'Apple', 'Mango']