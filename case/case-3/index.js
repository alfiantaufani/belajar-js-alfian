// Konfirmasi orangtua v1 
konfirmasi()

function konfirmasi() {
  let nilai1 = Math.floor(Math.random() * 9) + 1;
  let nilai2 = Math.floor(Math.random() * 9) + 1;
  document.getElementById("pertanyaan").innerHTML = `${nilai1} x ${nilai2}`; 

  let a =  [...Array(9).keys()].sort(() => Math.random() - 0.5); // acak 0 - 9
  let perkalian = nilai1 * nilai2;
  a.push(perkalian) // tambahkan ke atas array b jadi 10 array
  
  let x = a.splice(9, 2) // tak ambil array ke 10
  let randomJawaban = a.sort(() => Math.random() - 0.5).slice(0,3) // ambil 3 data
  
  let posisiJawaban = Math.floor(Math.random() * 3);
  let html = "";
  for (let i = 0; i < randomJawaban.length; i++) {
    if (posisiJawaban == i) {
       html += ` <button onclick="cek(this)" data-nilai1="${nilai1}" data-nilai2="${nilai2}" data-jawaban="${perkalian}">${perkalian}</button> `;
    }else{
       html += ` <button onclick="cek(this)" data-nilai1="${nilai1}" data-nilai2="${nilai2}" data-jawaban="${randomJawaban[i]}">${randomJawaban[i]}</button> `;
    }
  }

  document.getElementById("pilihan").innerHTML = html
}

function cek(e) {
  let hasil = parseInt(e.dataset.nilai1) * parseInt(e.dataset.nilai2)
  if (hasil == parseInt(e.dataset.jawaban)) {
    konfirmasi()
  } else {
    alert("Salah")
  }
}