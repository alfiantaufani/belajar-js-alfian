// Konfirmasi orangtua v2
konfirmasi()

function konfirmasi() {
    let nilai1 = Math.floor(Math.random() * 9) + 1;
    let nilai2 = Math.floor(Math.random() * 9) + 1;
    document.getElementById("pertanyaan").innerHTML = `${nilai1} x ${nilai2}`; 

    let perkalian = [nilai1 * nilai2];
    // console.log(perkalian)
    while (perkalian.length < 4) {
        perkalian.push(Math.floor(Math.random() * 100) + 1)
        perkalian = [...new Set(perkalian)]
    }
    perkalian.sort();
    // console.log(perkalian)
    
    let html = "";
    for (let i = 0; i < perkalian.length; i++) {
        if (perkalian[i] == nilai1 * nilai2) {
            html += ` <button onclick="konfirmasi()">${perkalian[i]}</button> `;
        }else{
            html += ` <button onclick="alert('Salah')">${perkalian[i]}</button> `;
        }
    }

    document.getElementById("pilihan").innerHTML = html
}