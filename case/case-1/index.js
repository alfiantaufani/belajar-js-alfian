/* 
    Sebuah jalan menginginkan kendaraan yang ber nomor plat ganjil 
    melewati jalur kiri.
*/
// random plat nomor
let plat_motor = Math.floor(Math.random() * 100) + 1;
 
class Motor {
    constructor(nama){
        this.nama = nama
    }
    randomMotor(){
        const motor = ['Vario', 'Beat', 'Mio', 'Scoopy'];
        const random = Math.floor(Math.random() * motor.length);
        return motor[random];
    }
    cek(param){
        this.param = param
        if (this.param % 2 === 0) {
            this.genap(this.param)
        } else {
            this.ganjil(this.param)
        }
    }
    ganjil(param){
        console.log(`Motor ${this.nama} dengan plat nomor ${param} bernilai ganjil`)
    }
    genap(param){
        console.log(`Motor ${this.nama} dengan plat nomor ${param} bernilai genap`)
    }
}

let nama_motor = [];
let mobil = new Motor(nama_motor)
nama_motor = nama_motor.push(mobil.randomMotor())
mobil.cek(plat_motor)