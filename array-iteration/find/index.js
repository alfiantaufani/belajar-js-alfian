console.log("Contoh Fungsi find() mencari sebuah data dalam array sesuai kondisi yang di tentukan. yang dikembalikan adalah value pertama yang ditemukan dalam sebuah array");

console.log("Contoh find biasa :")
const angka = [4, 9, 16, 25, 29];
let result = angka.find(function (value) {
    return value > 18
})
console.log(result + " ini value")

console.log("Contoh findIndex :")
let findIndex = angka.findIndex(function (value) {
    return value > 18
})
console.log(findIndex + " ini index value")