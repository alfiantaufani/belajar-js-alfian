console.log("Membuat maping data agara tiap data bisa diberkian fungsi tertentu")

console.log("Model 1")
const angka = [1,2,3,6];
const maping = angka.map(fungsi);

function fungsi(value, index, array) {
    return value * 2
}
console.log(angka)
console.log(maping) // hasil [2, 4, 6, 12]

// ==========================================================
console.log("Model 2")
const maping2 = angka.map(function(value){
    return value * value;
});

console.log(maping2) // [1, 4, 9, 36]

// ==========================================================
console.log("Model 3 dengan array object")
let data = [
    {nama : "Dani", jk : "L"},
    {nama : "Elsa", jk : "P"},
    {nama : "Yanto", jk : "L"},
    {nama : "Dian", jk : "L"},
    {nama : "Diana", jk : "P"},
]

let laki = [];
let pr = [];
let lakilaki = data.map(function(elem){
    if (elem.jk == 'L') {
        laki.push(elem.nama)
    }else{
        pr.push(elem.nama)
    }
});
  
console.log(laki, pr)// lakilaki akan berisi [{nama: 'Ali', jk: 'L' },{nama: 'Budi', jk: 'L' }];