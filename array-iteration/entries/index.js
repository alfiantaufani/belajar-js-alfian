console.log("Contoh fungsi entries() menghasilkan sebuah Array Iterator yang berisi key sekaligus value nya");
const buah = ["Banana", "Orange", "Apple", "Mango"];
const result = buah.entries();

for (let data of result) {
  console.log(data);
}
console.log(result)