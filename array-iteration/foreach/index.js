// forEach() 
console.log("Membuat foreach dengan meberikan fungsi tiap data array")
const number = [45, 4, 9, 16, 25];
const car = ["avanza", "brio", "xpander", "pajero", "innova"];

console.log("Model 1")
let txt = "";
number.forEach(myFunction);
function myFunction(value, index, array) {
    txt += + value + ",";
}
console.log(txt) // 45,4,9,16,25,

console.log("Model 2");
number.forEach(index);
function index(value, index, array) {
    console.log(index, value);
}

console.log("Model 3")
car.forEach(function (value, index, array) {
    console.log(index, value)
})