/* 
    Method array chaining adalah menggabungkan lebih 
    dari 1 method dalam mencari data array.
*/

let angka = [-2, 3, -6, 8, 4, 5, -5, 1, 9, -7, 7];
let chaining = angka.filter(a => a > 3) // memfilter angka lebih dari 3
    .map(a => a * 3) // hasil filter di kali 3
    .map(a => a / 2) // hasil map diatas di bagi 3
    .reduce((accumulator, currentValue) => accumulator + currentValue) // di jumlahkan semua
console.log(chaining) // hasilnya 49.5