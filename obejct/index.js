let car = "Fiat";

let cars = {type:"Fiat", model:"500", color:"white"};

console.log(cars.model) // 500
console.log(cars["model"]) // 500


const persons = {
    firstName: "John",
    lastName : "Doe",
    id       : 5566,
    tempat_lahir: "Jombang",
    tanggal_lahir: "2 Januari 2023",
    fullName : function() {
      return this.firstName + " " + this.lastName;
    },
    ttl: function(){
        return `${this.tempat_lahir}, ${this.tanggal_lahir}`
    }
};
console.log(persons.ttl()) // Jombang, 2 Januari 2023