// Format Date String Lengkap
console.log(new Date("October 13, 2014 11:13:00"));
// Fomat Date String Standard
console.log(new Date("2022-03-25"));

console.log("Convert to String :")
const tanggal = new Date();
console.log(tanggal.toString()) // Mon Oct 23 2023 13:36:36 GMT+0700 (Western Indonesia Time)
console.log(tanggal.toDateString()) // Mon Oct 23 2023
console.log(tanggal.toUTCString()) // Mon, 23 Oct 2023 06:36:58 GMT
console.log(tanggal.toISOString()) // 2023-10-23T06:37:06.812Z