const tanggal = new Date("2023-10-23");
const months = ["Januari", "Februari", "Maret", "April", "Mai", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];
const days = ["Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu"];

console.log("Ambil Bulan angaka dan stringnya");
console.log("Bulan " + tanggal.getMonth() + " " + months[tanggal.getMonth()]);
console.log("Ambil Hari angaka dan stringnya");
console.log("Hari " + tanggal.getDate() + " " + days[tanggal.getDay()])