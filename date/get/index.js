const tanggal = new Date();

console.log(tanggal.getFullYear()); // tahun
console.log(tanggal.getMonth()); // bulan
console.log(tanggal.getDate()); // tangal
console.log(tanggal.getDay()); // hari
console.log(tanggal.getHours()); // jam
console.log(tanggal.getMinutes()); // menit
console.log(tanggal.getSeconds()); // detik
console.log(tanggal.getMilliseconds()); //mili detik