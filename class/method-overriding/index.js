//  deklarasi kelas Master atau Umum
class Kendaraan {
    constructor(nama){
        this.nama = nama;
    }
    start(kecepatan) {
        this.kecepatan = kecepatan;
        console.log(`kendaraan ${this.nama} melaju dengan kecepatan ${this.kecepatan}`)
    }
    stop(){
        console.log(`kendaraan ${this.nama} berhasil berhenti`);
    }
}

// deklarasi kelas turunan
class Mobil extends Kendaraan {
    menang(){
        super.stop();
        this.lampu()
    }

    lampu() {
        console.log(`Lampu Mobil ${this.nama} berhasil menyala`)
    }
}

let mobil = new Mobil("Brio")
// mobil.start(20)
mobil.menang()