class Motor {
    constructor(nama, tahun){ // ketentuan dari class Motor
       this.nama = nama;
       this.tahun = tahun;
    }
}

const motor1 = new Motor("Vario", 2025)
const motor2 = new Motor("Beat", 2015)
console.log(motor1)
console.log(motor2)