//  deklarasi kelas Master atau Umum
class Kendaraan {
    constructor(nama){
        this.nama = nama;
    }
    run(kecepatan) {
        this.kecepatan = kecepatan;
        console.log(`kendaraan ${this.nama} melaju dengan kecepatan ${this.kecepatan}km`)
    }
    stop(){
        console.log(`kendaraan ${this.nama} berhasil berhenti`);
    }
}

class Motor extends Kendaraan {
    lampu() {
        console.log(`Lampu ${this.nama} berhasil dimatikan`)
    }
}

let motor = new Motor("Vario")
motor.run(30);
motor.stop();
motor.lampu();