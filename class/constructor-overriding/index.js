console.log("Constructor Overridig berfungsi untuk menggabungkan 2 constructor ke dalam 1 class")

// class ke 1
class Class1 {
    constructor(siswa, kelas){
        this.siswa = siswa;
        this.kelas = kelas;
    }
}

// menggabungkan class 1 ke walikelas dengan extends
class WaliKelas extends Class1 {
    constructor(s, k, guru){
        super(s, k); // a b perwakilan parameter dari siswa, kelas yg ada di Class1
        this.guru = guru;
    }
}
let class2 = new WaliKelas("Andi", 3, "Bu Guru")
console.log(class2)