//  deklarasi kelas Master atau Umum
class Kendaraan {
    constructor(tahun){
        this.tahun = tahun;
    }
    static motor(){
        return "Ini motor"
    }
    static mobil(params) {
        return "Ini mobil " + params
    }
    static tahun(x) {
        return "Ini mobil tahun " + x.tahun
    }
}

let kendaraan = new Kendaraan("2016");

console.log(kendaraan)
console.log(Kendaraan.motor()) // Ini motor
console.log(Kendaraan.mobil("Brio")) // Ini mobil Brio
console.log(Kendaraan.tahun(kendaraan)) // Ini mobil Brio