class Motor {
    constructor(nama, tahun){
      this.nama = nama;
      this.tahun = tahun;
    }
    umur() {
        const tanggal = new Date();
        return tanggal.getFullYear() - this.tahun;
    }
}
const motor = new Motor("Vario", 2014)
console.log(motor) // Motor {nama: 'Vario', tahun: 2015}
console.log("Motor ini ber umur " + motor.umur() + " tahun") // 9