class Motor {
    constructor(nama, tahun){
       this.nama = nama;
       this.tahun = tahun;
    }

    run(){
        this.variabel = "Contoh variable";
        return `Motor ${this.nama} dibuat pada tahun ${this.tahun}`;
    }
}
export {Motor};