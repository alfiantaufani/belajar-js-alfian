import { Motor } from "./import-class.js";

const motor = new Motor("Vario", 2014);

// akses semua
console.log(motor) // Motor {nama: 'Vario', tahun: 2014}

// akses this.nama
console.log(motor.nama) // Vario

// akses this.tahun
console.log(motor.tahun) // 2014

// akses fungsi dalam class
console.log(motor.run())

// akses variable yng ada didalam fungsi
console.log(motor.variabel) // Contoh variable