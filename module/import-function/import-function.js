const fungsi = function () {
    const nilai1 = 2;
    const nilai2 = 3;
    return nilai1 + nilai2;
}

function motor() {
    const nama = "Scoppy";
    const tahun = 2019;
    return `Motor ${nama} sejak tahun ${tahun}`;
}

export {fungsi, motor};
// export default motor;

/* 
    kalau yg ingin di export hanya satu, gunakan :
    export default name_function
*/